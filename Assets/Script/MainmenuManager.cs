using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainmenuManager : MonoBehaviour
{
    [SerializeField] private bool _isWebBuild = true;
    [SerializeField] private Animator _menuAnimator;
    public bool isWebBuild => _isWebBuild;

    private void Update()
    {
        if (Input.anyKeyDown && !SceneLoader.Instance.transitioning)
        {
            StartCoroutine("EnterScene");
        }
    }

    IEnumerator EnterScene()
    {
        _menuAnimator.SetTrigger("Enter");

        yield return new WaitForSeconds(0.5f);

        SceneLoader.Instance.startLoadLevel(1);

        yield return null;
    }
}
