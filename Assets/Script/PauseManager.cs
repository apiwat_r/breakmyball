using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    public bool isPaused = false;
    [SerializeField] private GameObject _PauseCanvas;

    private void Start()
    {
        StartUpSetup();
    }
    private void Update()
    {
        OpenCloseMenuTab();
    }

    void StartUpSetup()
    {
        _PauseCanvas.SetActive(false);
    }

    private void OpenCloseMenuTab()
    {
        if (SceneLoader.Instance.transitioning == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }
    private void Pause()
    {
        _PauseCanvas.SetActive(true);
        isPaused = true;
        Time.timeScale = 0.0f;
    }
    private void Resume()
    {
        _PauseCanvas.SetActive(false);
        isPaused = false;
        Time.timeScale = 1f;
    }
    public void ButtonResume()
    {
        Resume();
    }
    public void LoadScene(int newScene)
    {
        _PauseCanvas.SetActive(false);
        SceneLoader.Instance.startLoadLevel(newScene);
    }

}
