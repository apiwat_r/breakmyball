using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundHolder", menuName ="SoundHolder")]
public class SoundHolder : ScriptableObject
{
    public Sound[] soundArray;
}
