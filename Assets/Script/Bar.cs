using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
{
    [SerializeField] private Image slider;
    [SerializeField] private float maxGauge = 20;
    public float currentGauge { get; private set; } = 0;
    [SerializeField] private float time = 0;
    // Start is called before the first frame update

    private void OnEnable()
    {
        currentGauge = 0;

    }

    private void OnDisable()
    {
        LeanTween.cancelAll();
        currentGauge = 0;
    }
    // Update is called once per frame
    void Update()
    {
        slider.fillAmount = currentGauge / maxGauge;
    }

    public void startCharge()
	{
        LeanTween.value(currentGauge, maxGauge, time).setLoopPingPong(-1).setOnUpdate((float val) => { currentGauge = val; });
    }

    public float cancelCharge()
	{
        LeanTween.cancelAll();
        return currentGauge;
        
	}

    public void resetCharge()
	{
        currentGauge = 0;
	}
}
