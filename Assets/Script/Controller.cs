using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Controller : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] float timer;
    [SerializeField] TextMeshProUGUI timerText;

    [Header("Bar")]
    [SerializeField] Bar barLeft;
    [SerializeField] Bar barRight;
    [SerializeField] GameObject barContentLeft;
    [SerializeField] GameObject barContentRight;

    [Header("Egg")]
    [SerializeField] float eggHealthLeft;
    [SerializeField] float eggHealthRight;
    [SerializeField] float eggSpamHealth;
    [SerializeField] GameObject egg;
    [SerializeField] private float forceDiviser = 1;
    [SerializeField] private List<GameObject> crackTexture = new List<GameObject>();
    [SerializeField] private List<GameObject> lightRay = new List<GameObject>();
    [SerializeField] private List<ParticleSystem> particleSystems = new List<ParticleSystem>();

    [Header("Reference object")]
    [SerializeField] private Camera camera;
    [SerializeField] private GameObject spaceText;
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject whiteScreen;
    [SerializeField] private GameObject buttonSort;
    

    [SerializeField] private GameObject chicken;
    
    Rigidbody2D eggRB;
    private bool enablePush = true;
    private bool enableSpam;
    private bool phase2 = false;
    private bool gameLose = false;
    private bool gameWin = false;
    
    

	private void Start()
	{
        eggRB = egg.GetComponent<Rigidbody2D>();
        LeanTween.move(spaceText.GetComponent<RectTransform>(), new Vector3(0, -155, 0), 1f);
        spaceText.GetComponent<TextMeshProUGUI>().text = "Push the egg! \n Don't let it fall off though!";
    }

	void Update()
    {

        // reseting the scene
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("SampleScene");
        }

        if (gameLose|| gameWin)
		{
			if (Input.anyKeyDown)
			{
                
			}
            return;
		}

        timerCount();

        Pushing();

        Spaming();

        lightRay[3].transform.Rotate(0, 0, 10 * Time.deltaTime);
    
    }

    public void EggSpamEnable()
	{
        enableSpam = true;
        eggRB.gravityScale = 0;
	}

    public float calforce(float input)
	{
        input = Mathf.Clamp(input,7f, 15f);
        return input;
	}

    //shake the object based on set duration and magnitude
    public IEnumerator Shake(float duration, float magnitude, GameObject obj = null)
	{
        if(obj == null)
		{
            Debug.LogError(obj + "not detected");
            yield return null;
        }

        Vector3 originalPos = obj.transform.position;

        float elapsed = 0.0f;

        while(elapsed < duration)
		{
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            obj.transform.position = new Vector3(x, y, originalPos.z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        obj.transform.position = originalPos;
	}

    // toggle bar charge based on key pressed
    public void Pushing()
	{
        if (enablePush)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                barLeft.startCharge();
            }

            if (Input.GetKeyUp(KeyCode.A))
            {

                AudioManager.instance.Play("Push");
                eggHealthLeft -= barLeft.cancelCharge();
                eggRB.AddForce(Vector2.left * (calforce(barLeft.cancelCharge()) / forceDiviser), ForceMode2D.Impulse);
                barLeft.resetCharge();

                if (eggHealthLeft < 20)
                {
                    eggCrack(0);
                }

                if (eggHealthLeft < 0)
                {
                    eggCrack(1);
                }
            }


            if (Input.GetKeyDown(KeyCode.D))
            {
                barRight.startCharge();
            }

            if (Input.GetKeyUp(KeyCode.D))
            {
      
                AudioManager.instance.Play("Push");
                eggHealthRight -= Mathf.Clamp(barRight.cancelCharge(),1,20);  
                eggRB.AddForce(Vector2.right * (calforce(barRight.cancelCharge()) / forceDiviser), ForceMode2D.Impulse);
                barRight.resetCharge();

                if (eggHealthRight < 20)
                {
 
                    eggCrack(2);
                }

                if (eggHealthRight <= 0)
                {

                    eggCrack(3);
                }
            }
        }
    }

    //Space bar spam check
    public void Spaming()
	{
        if (eggHealthLeft <= 0 && eggHealthRight <= 0)
        {
            enablePush = false;
            eggRB.velocity = new Vector3(0, 0, 0);
            spaceText.SetActive(true);
            egg.GetComponent<CapsuleCollider2D>().enabled = false;
            if (!LeanTween.isTweening() && !phase2)
            {
                eggRB.freezeRotation = true;
                LeanTween.move(barContentLeft.GetComponent<RectTransform>(), new Vector3(-530, -23, barContentLeft.transform.position.z), 1f);
                LeanTween.move(barContentRight.GetComponent<RectTransform>(), new Vector3(530, -23, barContentRight.transform.position.z), 1f);
                LeanTween.move(spaceText.GetComponent<RectTransform>(), new Vector3(0, -291, 0), 1f).setOnComplete(changeText, "Break the egg! \n Spam your space bar");
                LeanTween.move(egg, new Vector3(0f, -0.5f, 0f), 1f).setEase(LeanTweenType.easeInOutCubic).setOnComplete(EggSpamEnable);

                LeanTween.rotate(egg, new Vector3(0,0,0), 1f).setEase(LeanTweenType.easeInOutCubic);
                phase2 = true;

            }

            if (enableSpam)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    AudioManager.instance.Play("Push");
                    StartCoroutine(Shake(0.1f, 0.5f,egg));
                    StartCoroutine(Shake(0.1f, 0.5f, camera.gameObject));

                    eggSpamHealth -= 1;

                    particleSystems[2].Play();

                    if(eggSpamHealth <= 32)
					{
                        checkLight(0);
					}

                    if(eggSpamHealth <= 25)
					{
                        checkLight(1);
                    }

                    if (eggSpamHealth <= 17)
                    {
                        checkLight(2);
                    }

                    if (eggSpamHealth <= 10)
                    {
                        checkLight(3);
                    }

                    if (eggSpamHealth <= 0)
					{
                        enableSpam = false;
                        gameWin = true;
                        timerText.text = "";
                        AudioManager.instance.Play("Boom");
                        whiteScreen.SetActive(true);
                        chicken.SetActive(true);
                        egg.SetActive(false);
                        for (int i = 0; i < lightRay.Count; i++)
						{
                            lightRay[i].SetActive(false);
						}
                        LeanTween.move(chicken, chicken.transform.position + new Vector3(0, 0.5f, 0), 1f).setLoopPingPong(-1);
                        LeanTween.alpha(whiteScreen.GetComponent<RectTransform>(), 0, 1f).setDelay(3f).setOnComplete(chip);
                        LeanTween.move(buttonSort.GetComponent<RectTransform>(), new Vector3(0, 0, barContentRight.transform.position.z), 1f).setEase(LeanTweenType.easeOutQuad);
                        spaceText.GetComponent<TextMeshProUGUI>().text = "You hatch a little happy chicken!";

					}
                }
            }
        }
    }

    public void chip()
	{
        AudioManager.instance.Play("Chicken");
        whiteScreen.SetActive(false);
        
	}

    public void changeText(object text)
	{
        string d = text.ToString();
        LeanTween.move(spaceText.GetComponent<RectTransform>(), new Vector3(0, -155, 0), 1f);
        spaceText.GetComponent<TextMeshProUGUI>().text = d;
    }

    //check egg health
    public void eggCrack(int index)
	{
        if (!crackTexture[index].activeSelf)
        {
            if(index == 0  || index == 1)
			{
                particleSystems[0].Play();
            }
            if (index == 2 || index == 3)
            {
                particleSystems[1].Play();
            }
            AudioManager.instance.Play("EggCrack");
            crackTexture[index].SetActive(true);
        }
    }

    public void checkLight(int index)
	{
        if (!lightRay[index].activeSelf)
        {
            AudioManager.instance.Play("Light");
        }
        lightRay[index].SetActive(true);
    }

    public  void timerCount()
	{
        timer -= Time.deltaTime;
        timerText.text = timer.ToString("0");
        timer = Mathf.Clamp(timer, 0f, 60f);

        if(timer <= 0)
		{
            gameLose = true;
            LeanTween.move(barContentLeft.GetComponent<RectTransform>(), new Vector3(-530, -23, barContentLeft.transform.position.z), 1f);
            LeanTween.move(barContentRight.GetComponent<RectTransform>(), new Vector3(530, -23, barContentRight.transform.position.z), 1f);
            LeanTween.move(spaceText.GetComponent<RectTransform>(), new Vector3(0, -291, 0), 1f).setOnComplete(changeText, "The egg gone cold :(");
            LeanTween.move(buttonSort.GetComponent<RectTransform>(), new Vector3(0, 0, barContentRight.transform.position.z), 1f).setEase(LeanTweenType.easeOutQuad);
            LeanTween.color(egg, new Color32(57,111,135,255), 1f).setEaseOutQuad();
  
        }
	}

    public void falloff()
	{
        gameLose = true;
        LeanTween.move(barContentLeft.GetComponent<RectTransform>(), new Vector3(-530, -23, barContentLeft.transform.position.z), 1f);
        LeanTween.move(barContentRight.GetComponent<RectTransform>(), new Vector3(530, -23, barContentRight.transform.position.z), 1f);
        LeanTween.move(buttonSort.GetComponent<RectTransform>(), new Vector3(0, 0, barContentRight.transform.position.z), 1f).setEase(LeanTweenType.easeOutQuad);
        LeanTween.move(spaceText.GetComponent<RectTransform>(), new Vector3(0, -291, 0), 1f).setOnComplete(changeText, "The egg fall off :(");
    }
	private void OnDestroy()
	{
        AudioManager.instance.Stop("Chicken");
	}


}
