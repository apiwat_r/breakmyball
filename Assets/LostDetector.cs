using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LostDetector : MonoBehaviour
{
	[SerializeField] Controller controller;


	private void OnTriggerEnter2D(Collider2D collision)
	{
		controller.falloff();
	}
}
