// This file is auto-generated. Do not modify or move this file.

public static class BuildConstants
{
    public enum ReleaseType
    {
        None,
        Prototype,
    }

    public enum Platform
    {
        None,
        WebGL,
        PC,
    }

    public enum Architecture
    {
        None,
        WebGL,
        Windows_x86,
    }

    public enum Distribution
    {
        None,
        DistributionName,
    }

    public static readonly System.DateTime buildDate = new System.DateTime(637741685700825819);
    public const string version = "1.0.0.7";
    public const ReleaseType releaseType = ReleaseType.Prototype;
    public const Platform platform = Platform.WebGL;
    public const Architecture architecture = Architecture.WebGL;
    public const Distribution distribution = Distribution.DistributionName;
}

